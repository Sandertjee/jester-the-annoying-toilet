<?php
$servername = "localhost";
$username = "USERNAME";
$password = "PASSWORD";
$dbname = "jester";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



// User key is de key gegenereerd door het script wanneer iemand binnen komt lopen, dit is zijn/haar key voor de session
// Scenario is hoe we het scenario noemen wat afspeeld
// user response in wat de user zegt in text
// walkedinorout true = walked out false = walked in


// Scenario toevoegen
// https://sandervanderburgt.com/speech-to-text/dbcall?user_key= USER KEY &scenario= SCENARIO &user_response= RESPONSE &walkedinorout=

// user toevoegen / stoppen
// https://sandervanderburgt.com/speech-to-text/dbcall?user_key=&scenario=&user_response=&walkedinorout= TRUE OR FALSE




//echo uniqid();
//5be002b189fc7

$currenttimestamp = date("Y-m-d H:m:s");



if(isset($_GET["user_key"]) && isset($_GET["scenario"]) && isset($_GET["user_response"]) && isset($_GET["walkedinorout"])){
    $userkey = $_GET["user_key"];
    $scenario = $_GET["scenario"];
    $user_response = $_GET["user_response"];
    $walkedinorout =  $_GET["walkedinorout"];


    if(!empty($walkedinorout)){
        $checkkey = $conn->prepare('SELECT * FROM users WHERE user_key = ?');
        $checkkey->bind_param('s', $userkey);
        $checkkey->execute();
        $checkkeyresult = $checkkey->get_result();


        if($checkkeyresult->num_rows <= 0) {
            $createuser = $conn->prepare("INSERT INTO users (user_key, walked_In) VALUES (?, now())");
            $createuser->bind_param("s", $userkey);
            $createuser->execute();

            echo "Created user";

        }
        elseif($walkedinorout == "true"){
            $updateuser = $conn->prepare("UPDATE users SET walked_Out = now() WHERE user_key = ?");
            $updateuser->bind_param("s", $userkey);
            $updateuser->execute();

            echo "Updated user";
        }

    }

    if(!empty($scenario) && !empty($user_response)){

        echo "created response";

        $insertresponse = $conn->prepare("INSERT INTO responses (user_key, scenario, user_response) VALUES (?, ?, ?)");
        $insertresponse->bind_param("sss", $userkey, $scenario, $user_response);
        $insertresponse->execute();
    }
}




?>