let searchUrl = 'https://nl.wikipedia.org/w/api.php?action=opensearch&format=json&search=' ;


function setup(){
  noCanvas();
  userInput = select('#userInput');
  userInput.changed(goWiki);
  goWiki();


  function goWiki(){
    let term = userInput.value();
    let url = searchUrl + term;
    loadJSON(url, getData, 'jsonp');
  }

  function getData(data){
    console.log(data);
  }
}
