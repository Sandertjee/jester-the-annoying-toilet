let searchUrl = 'https://nl.wikipedia.org/w/api.php?action=opensearch&format=json&search=' ;

let userInput;

function setup(){
    noCanvas();
    userInput = select('#userInput');
    userInput.changed(goWiki);
    goWiki();


    function goWiki(){
        let term = userInput.value();
        let url = searchUrl + term;
        loadJSON(url, getData, 'jsonp');
    }

    function getData(data){
        console.log(data);
    }
}

// https://nl.wikipedia.org/w/api.php?format=jsonfm&action=query&prop=extracts&titles=bij&redirects=true&explaintext&exintro
