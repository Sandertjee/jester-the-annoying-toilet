
var button;
var song;
var amp;
var volhistory = [];
var amphistory = [];
var mic;

//
// function toggleSong(){
//     if (song.isPlaying()) {
//         song.pause();
//     }   else {
//         song.play();
//         }
//     }

function preload(){
    // song = loadSound('ikson.mp3')
}

function setup() {
    createCanvas(400, 400);
    //button = createButton('toggle');
    //button.mousePressed(toggleSong);
    //song.play();
    // amp = new p5.Amplitude();
    mic = new p5.AudioIn();
    mic.start();
}

function draw() {
    background(0);

    //verander var vol naar amp.level voor muziek te visualiseren.
    var vol = mic.getLevel();

    volhistory.push(vol);

    stroke(255);
    noFill();
    beginShape();
    for (var i =0; i < volhistory.length; i++){
        var y = map(volhistory[i], 0, 1, 250, 0);
        vertex(i, y);
    }

    endShape();

    // zorgt ervoor dat de lijn doorloopt. begin verdwijnt.
    if(volhistory.length >= width-5){
        volhistory.splice(0, 1);

    }

    //rode lijn
    stroke(255, 0, 0);
    line(volhistory.length, 0, volhistory.length, height);

}


